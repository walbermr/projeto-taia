from datetime import date

class dayTrade:
	def __transform_date(self, str):
		new_date = str.split("-")
		new_date = date(int(new_date[0]), int(new_date[1]), int(new_date[2]))
		return new_date

	def __init__(self, params):
		self.date = self.__transform_date(params[0])
		self.open = float(params[1])
		self.high = float(params[2])
		self.low = float(params[3])
		self.close = float(params[4])
		self.volume = float(params[5])
		self.adj_close = float(params[6])

	def setAttributes(self, params):
		self.open = float(params[0])
		self.high = float(params[1])
		self.low = float(params[2])
		self.close = float(params[3])
		self.volume = float(params[4])
		self.adj_close = float(params[5])

	def printDayTrade(self):
		buf = []
		buf = str(self.date)
		buf += " "
		buf += str(self.open)
		buf += " "
		buf += str(self.high)
		buf += " "
		buf += str(self.low)
		buf += " "
		buf += str(self.close)
		buf += " "
		buf += str(self.volume)
		buf += " "
		buf += str(self.adj_close)
		buf += '\n'
		print(buf)

		return buf

	def getVector(self):
		return [self.open, self.high, self.low, self.close, self.volume, self.adj_close]
