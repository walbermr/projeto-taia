# -*- coding: utf-8 -*-
import random, math, numpy, datetime
from dayTradeClass import dayTrade
from pybrain.structure.connections import FullConnection
from pybrain.structure.networks import FeedForwardNetwork as ffn
from pybrain.structure.modules import SigmoidLayer as sl


FILE_EXTENTION = ".csv"

QUANTITY_COMPANIES = 1
QUANTITY_ATTRIBUTES = 6
POP_SIZE = 100
TRADES = None
TRAINING_TRADES = None
TRAINING_TARGETS = None
VALIDATION_TRADES = None
VALIDATION_TARGETS = None

INPUT_LAYER_QUANTITY = 6
HIDDEN_LAYER_QUANTITY = 3
OUTPUT_LAYER_QUANTITY = 1

NEURAL_NET = None
FITNESS_TYPE = 'sng'	#net usa uma rede neural completa, sng usa um unico neuronio

TARGET_DAY = 1

ITERATION_N = 0	#variavel para arquivo de debug
ALG_TYPE = 1	#tipo do algoritmo usado: 0 -> eaSimple da biblioteca, 1 -> nosso algoritmo genético
NGEN = 5000		#numero de gerações para o algoritmo
MUTPB = 0.5		#probabilidade de mutação
IND_ATTR_BOUND = 1000
CXPB = 0.9		#probabilidade de crossover
MUTWIDTH = 20	#tamanho da distribuição para a mutação

OUTPUT_ARQ = open("./output_arq", "w")

def getKey(item):
	return item[1]


def selBest(population, k):
	population.sort( key = getKey, reverse = True)
	return population[:k]


def initNet():
	global INPUT_LAYER_QUANTITY
	global HIDDEN_LAYER_QUANTITY
	global OUTPUT_LAYER_QUANTITY
	global NEURAL_NET
	global OUTPUT_ARQ

	NEURAL_NET = ffn()
	inpl = sl(INPUT_LAYER_QUANTITY)
	h1l = sl(HIDDEN_LAYER_QUANTITY)
	outl =  sl(OUTPUT_LAYER_QUANTITY)

	NEURAL_NET.addOutputModule(outl)
	NEURAL_NET.addInputModule(inpl)
	NEURAL_NET.addModule(h1l)

	NEURAL_NET.addConnection(FullConnection(inpl, h1l))
	NEURAL_NET.addConnection(FullConnection(h1l, outl))

	NEURAL_NET.sortModules()
	
	OUTPUT_ARQ.write("Quantidade de pesos: %d" %(len(NEURAL_NET.params)))

	return


def fitnessNeural(individual, trades, targets):
	global ITERATION_N
	ITERATION_N += 1
	fit = 0
	net_params = individual[:]

	#print(net_params)
	activation_vector = []
	NEURAL_NET._setParameters(net_params)

	for i in range(0, len(trades)):
		activation = NEURAL_NET.activate(trades[i].getVector())[0]
		activation_vector.append(activation)

		if activation >= 0.5 and targets[i] >= 0:	#indica crescimento ou estagnação da açao
			fit += 1
		elif activation < 0.5 and targets[i] < 0:	#indica decrescimento da ação
			fit += 1
		else:
			fit = fit

	fit = 0.001 if fit == 0 else fit
	#file_name = "../../../../debugfiles/it" + str(ITERATION_N) + ".o"
	#o_file = open(file_name, 'w')
	#o_file.write(str(activation_vector))
	#o_file.close()
	return fit

def normalization(trades):
	v = [[],[],[],[],[],[],[]]

	for i in range(1, QUANTITY_ATTRIBUTES+1):
		for j in range(0, len(trades)):
			if(i == QUANTITY_ATTRIBUTES):
				trades[j][i] = trades[j][i].split('\r')[0]

			v[i].append(float(trades[j][i]))

		v[i] = [((x-min(v[i]))/(max(v[i])-min(v[i]))) for x in v[i]]

	for i in range(1, QUANTITY_ATTRIBUTES+1):
		for j in range(0, len(trades)):
			trades[j][i] = v[i][j]

	return trades


def readTestFiles(co_num, is_complete_file, path, prefix, ext):
	trades = []
	file = open(path + prefix + str(co_num) + ext).readlines()

	start = 0 if is_complete_file else 1

	for i in range(start, len(file)):
		file[i] = file[i].split(",")

	file_normalized = normalization(file[start:len(file)])
	
	for i in range(0, len(file_normalized)):
		trades.append(dayTrade(file_normalized[i]))

	return trades

def sigmoid(x):
	ret = 0
	try: 
		ret = 1/(1+math.exp(-x))
	except OverflowError:
		if x < 0:
			ret = 0
		else:
			OUTPUT_ARQ.write("Something strange happened")
	return ret

def neuron(weights, input_data):
	return 1 if sigmoid(sum([a*b for a,b in zip(weights, input_data)])) >= 0.5 else 0

def createTargets(trades, test_trades, target_day):
	targets = []
	for i in range(0, len(test_trades)):

		j = trades.index(next((x for x in trades if x.date == test_trades[i].date), None))
		t = trades[j-target_day].close - test_trades[i].close
		targets.append(t)

	return targets

def fitnessSingleNeuron(individual, trades, targets):	# target = day[i] - day[i+1]
	fit = 0

	for i in range(0, len(trades)):
		activation = neuron(individual, trades[i].getVector())

		if activation == 1 and targets[i] >= 0:	#indica crescimento ou estagnação da açao
			fit += 1
		elif activation == 0 and targets[i] < 0:	#indica decrescimento da ação
			fit += 1
		else:
			fit = fit

	fit = 0.001 if fit == 0 else fit

	return fit

def printTopAndFitness(top):
	global OUTPUT_ARQ
	fitnesses = []

	for ind in top:
		fitnesses.append(ind[1])
		OUTPUT_ARQ.write(str(ind[0]) + "---" + str(ind[1]) + '\n')
	return fitnesses


def mutCreep(individual):
	global MUTPB
	global MUTWIDTH

	val = numpy.random.normal(scale = MUTWIDTH)
	for i in range(0, len(individual)):

		if random.random() < MUTPB:
			individual[0][i] += val

	return individual


def rouletteSelection(population, pesos, lamdba):
	pais = []
	current_number = 1

	while current_number <= lamdba:
		rand = random.random()
		i = 0

		while i < len(pesos) and pesos[i] < rand:
			i += 1

		if population[i] not in pais:
			pais.append(population[i])
			current_number += 1

	return pais


def roulette(population, k):
	prob = []
	acc_prob = []

	fn = float(sum([y for x, y in population]))
	prob = [f/fn for x, f in population]

	for i in range(0, len(prob)):
		acc_prob.append(sum(prob[0:i+1]))

	pais = rouletteSelection(population, acc_prob, k) #seleciona dois pais

	return pais


def cutAndCrossfill(p1, p2): 
	f1 = ([], 0)
	f2 = ([], 0)
	cut1 = cut2 = random.randint(0, len(p1[0]))

	while cut1 == cut2:
		cut2 = random.randint(0, len(p1[0]))

	if(cut2 < cut1):
		cut1 , cut2 = cut2, cut1

	for i in range(0, len(p1[0])):
		if i < cut1 or i >= cut2:
			f1[0].append(p1[0][i])
			f2[0].append(p2[0][i])
		elif i < cut2:
			f1[0].append(p2[0][i])
			f2[0].append(p1[0][i])

	return (f1, f2)


def select(population, k):	#seleção de pais
	pais = []
	pais = roulette(population, k)
	return pais


def calcFitness(population, trades, targets):

	for i in range(0, len(population)):
		population[i] = fitness(population[i], trades, targets)

	return population

def gaSimple(population, cxpb, mutpb, ngen):
	global TRAINING_TRADES
	global TRAINING_TARGETS
	global OUTPUT_ARQ

	s = []
	nevals = 0
	pop_size = len(population)
	OUTPUT_ARQ.write("Started genetic algorithm... Creating Population fitness\n")
	OUTPUT_ARQ.write("Starting algorithm.\n")
	OUTPUT_ARQ.write("melhor fitness\tmedia\tdesvio padrao\n")	

	for i in range(0, ngen):
		nevals = 0
		invalid_ind = []
		parents = select(population, k=2)	#seleciona apenas 2 pais

		#crossover
		if random.random() < cxpb:
			s = cutAndCrossfill(parents[0], parents[1])	#gera 2 filhos no cruzamento
			
			#considera apenas 2 filhos na mutação
			for j in range(0, 2):
				#mutação apenas nos filhos
				ind = ([],0)
				ind = mutCreep(s[j])
				ind = calcFitness([ind], TRAINING_TRADES, TRAINING_TARGETS)[0]
				population.append(ind)
				nevals += 1

		#seleção da nova população
		population = selBest(population, k=pop_size)

		print("%d\t%d" %(i, nevals))
		pop_fit = [ind[1] for ind in population]	
		print(pop_fit)
		#print(tools.selBest(population, k=1))
		OUTPUT_ARQ.write("%f\t%f\t%f\t\n" %(selBest(population, k=1)[0][1], numpy.mean(pop_fit), numpy.std(pop_fit)))
	return population


def countIncDec(target):
	global OUTPUT_ARQ

	inc = 0.0
	dec = 0.0

	for i in range(0, len(target)):
		if target[i] >= 0:
			inc+=1
		else:
			dec+=1
	OUTPUT_ARQ.write("%d\n" %(inc))
	OUTPUT_ARQ.write("%d\n" %(dec))
	inc = inc/len(target)
	dec = dec/len(target)
	return inc, dec


def fitness(ind, trades, targets):
	global FITNESS_TYPE

	if(FITNESS_TYPE == "net"):
		return (ind[0], fitnessNeural(ind[0], trades, targets))

	elif(FITNESS_TYPE == "sng"):
		return (ind[0], fitnessSingleNeuron(ind[0], trades, targets))
	else:
		return 0

def genPopulaion(n, size_individual, individual_bounds):
	global TRAINING_TRADES
	global TRAINING_TARGETS
	population = []

	while len(population) < n:
		individuo = []
		while len(individuo) < size_individual:
			gene = random.uniform(-individual_bounds, individual_bounds)
			individuo.append(gene)

		tup = fitness((individuo, 0), TRAINING_TRADES, TRAINING_TARGETS)

		if tup not in population:
			population.append(tup)
	return population


def main():
	global TRADES
	global ALG_TYPE
	global IND_ATTR_BOUND
	global TRAINING_TARGETS
	global QUANTITY_COMPANIES
	global VALIDATIONS
	global TRAINING_TRADES
	global VALIDATION_TRADES
	global FITNESS_TYPE
	global toolbox
	global OUTPUT_ARQ

	global NGEN
	global CXPB
	global MUTPB

	for i in range(1,QUANTITY_COMPANIES+1):
		population = []

		TRADES = readTestFiles(i, False, "csv_files/", "file", FILE_EXTENTION)

		TRAINING_TRADES = readTestFiles(i, True, "csv_files/files_validate/", "file_validate", FILE_EXTENTION)
		TRAINING_TARGETS = createTargets(TRADES, TRAINING_TRADES, TARGET_DAY)

		VALIDATION_TRADES = readTestFiles(i, True, "csv_files/files_test/", "file_test", FILE_EXTENTION)
		VALIDATION_TARGETS = createTargets(TRADES, VALIDATION_TRADES, TARGET_DAY)

		#TRAINING_TRADES = readTestFiles(i, True, "csv_files/files_test/", "file_test", FILE_EXTENTION)
		#TRAINING_TARGETS = createTargets(TRADES, TRAINING_TRADES, TARGET_DAY)

		#VALIDATION_TRADES = readTestFiles(i, True, "csv_files/files_validate/", "file_validate", FILE_EXTENTION)
		#VALIDATION_TARGETS = createTargets(TRADES, VALIDATION_TRADES, TARGET_DAY)


		OUTPUT_ARQ.write("Parametros:\nNGEN: %d CXPB: %f MUTPB: %f\tpasso:(até)%f\nINICIALIZACAO: -%d %d\nALGTYPE: %d\n FITNESS_TYPE: %s\nTARGET_DAY: %d\nPOP_SIZE: %d\n" %(NGEN, CXPB, MUTPB, MUTWIDTH, IND_ATTR_BOUND, IND_ATTR_BOUND, ALG_TYPE, FITNESS_TYPE, TARGET_DAY, POP_SIZE))
		OUTPUT_ARQ.write("Tamanho TRADES: %d\n" %(len(TRADES)))
		OUTPUT_ARQ.write("Tamanho TARGETS: %d\n" %(len(TRAINING_TARGETS)))
		OUTPUT_ARQ.write("Tamanho TRAINING_TRADES: %d\n" %(len(TRAINING_TRADES)))

		if FITNESS_TYPE == 'net':
			print("Starting ANN...")
			initNet()
			print("ANN started.\nGenerating population...")
			population = genPopulaion(n=POP_SIZE, size_individual=len(NEURAL_NET.params), individual_bounds=IND_ATTR_BOUND)
			print("Population generated.")

		elif FITNESS_TYPE == 'sng':
			population = genPopulaion(n=POP_SIZE, size_individual=QUANTITY_ATTRIBUTES, individual_bounds=IND_ATTR_BOUND)
		
		top = selBest(population, k=POP_SIZE)
		fit = printTopAndFitness(top)
		
		OUTPUT_ARQ.write("Média de fitness: %f\n" %(numpy.mean(fit)))

		##ESCOLHA DO ALGORITMO##
		if ALG_TYPE == 0:
			pass
			#population, log = algorithms.eaSimple(population, toolbox, cxpb=CXPB, mutpb=MUTPB, ngen=NGEN, verbose=True)
		elif ALG_TYPE == 1:
			population = gaSimple(population, cxpb=CXPB, mutpb=MUTPB, ngen=NGEN)

		top = selBest(population, k=POP_SIZE)
		fit = printTopAndFitness(top)

		per = countIncDec(TRAINING_TARGETS)
		OUTPUT_ARQ.write("Porcentagem: INC=%f , DEC=%f.\n" %(per[0]*100, per[1]*100))
		OUTPUT_ARQ.write("Média de fitness: %f\n" %(numpy.mean(fit)))
		top = selBest(population, k=1)
		fit = printTopAndFitness(top)
		OUTPUT_ARQ.write("Melhor: %d %f\n" %(fit[0], fit[0]/1240.0))


		OUTPUT_ARQ.write("Validation:\n")
		population = calcFitness(population, VALIDATION_TRADES, VALIDATION_TARGETS)
		top = selBest(population, k=POP_SIZE)
		fit = printTopAndFitness(top)
		OUTPUT_ARQ.write("Média de fitness: %f\n" %(numpy.mean(fit)))
		per = countIncDec(VALIDATION_TARGETS)
		OUTPUT_ARQ.write("Porcentagem: INC=%f , DEC=%f.\n" %(per[0]*100, per[1]*100))
		top = selBest(population, k=1)
		fit = printTopAndFitness(top)
		OUTPUT_ARQ.write("Melhor fitness: %d , porcentagem: %f.\n\n\n\n\n\n\n\n" %(fit[0], fit[0]/1240.0))

if __name__ == "__main__":
	main()


	##PARA TESTAR##
	## COLOCAR MUTAÇÃO MENOR 10% E 90% ##
	## CAMADA ESCONDIDA: 2, 4 E 6 NEURONIOS ##