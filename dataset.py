import random
from random import shuffle

DATASET_SIZE = 1240

def create_dataset( ):
	
	for i in range(0, 8):
		file = open("csv_files/file" + str(i + 1) + ".csv", 'r').readlines( )
		file = file[2:len(file)]
		shuffle(file)

		file_validate = open("csv_files/files_validate/file_validate" + str(i + 1) + ".csv", 'w')
		file_test = open("csv_files/files_test/file_test" + str(i + 1) + ".csv", 'w')

		for i in range(0, DATASET_SIZE):
			file_validate.write(file[i])

		for  i in range(DATASET_SIZE + 1, (DATASET_SIZE * 2) + 1):
			file_test.write(file[i])

		file_validate.close( )
		file_test.close( )

	return

create_dataset( )