# -*- coding: utf-8 -*-
import numpy, random, math

c2 = 1.8	#constantes de proporcionalidade
c =  0.3
REPEAT = 1	#quantas vezes o algoritmo será repetido
N = 30	#quantidade de dimensões do domínio
E_0 = 0.00001	#epsilon zero
POPULATION = [([], [])]	#population[i] -> individuo. individuo[0] -> cromossomo, individuo[1] -> sigma
POP_SIZE = 200
MUTATION_OPTION = 2	#define qual o tipo da mutação usada case 1 ou case 2
CROSSOVER_OPTION = "mean"	#define qual o tipo de crossover "rand" ou "mean"
SURVIVAL_OPTION = "plus"	#define qual o tipo de seleção de sobreviventes "comma" ou "plus"

DEBUG = False	#habilita prints para debug do algoritmo

debug_file = open("debug.o", "w")
mean_file = open("mean.o", "w")	#arquivo recebe média dos fitness da população
min_file = open("min.o", "w")	#arquivo recebe fitness minimo da população

if(MUTATION_OPTION == 1):	#habilita qual arquvio será usado para escrita de dados para o relatório
	output_file = open("data1.o", "w")
elif(MUTATION_OPTION == 2):
	output_file = open("data2.o", "w")

def fitness(individuo):	#função de ackley
	global N
	soma1 = 0
	soma2 = 0
	
	for i in range(0, N):
		soma1 += math.pow(individuo[i], 2)
		soma2 += math.cos(2.0*math.pi*individuo[i])

	return -20.0*math.exp(-0.2*math.sqrt(soma1/N)) - math.exp(soma2/N) + 20 + math.e

def getKey(item):
	return fitness(item[0])

def printAllFitness():	#imprime na tela todos os fitness da população
	global POPULATION
	global debug_file
	
	for i in range (0, len(POPULATION)):
		debug_file.write("%s : %f\n" %(str(POPULATION[i]), fitness(POPULATION[i][0])))
	debug_file.write("\n")
	return

def commaSelection(new_individual):	#seleçãoo de sobreviventes (u,y)
	global POPULATION

	POPULATION.sort(key = getKey)
	
	if(DEBUG):
		printAllFitness()
	
	POPULATION.pop()
	POPULATION.append(new_individual)

	return

def plusSelection(new_individual):	#seleção de sobreviventes (u+y)
	global POPULATION

	POPULATION.append(new_individual)
	POPULATION.sort(key = getKey)
	
	if(DEBUG):
		printAllFitness()
	
	POPULATION.pop()

	return

def survivalSelection(new_individual):	#função direciona para o tipo de seleção de sobreviventes
	global SURVIVAL_OPTION
	global POPULATION
	global DEBUG
	global debug_file

	if(DEBUG):
		debug_file.write("survival selection antes:")

	if SURVIVAL_OPTION == "comma":
		commaSelection(new_individual)

	elif SURVIVAL_OPTION == "plus":
		plusSelection(new_individual)	

	if(DEBUG):
		debug_file.write("survival selection depois:")
		printAllFitness()
	
	return


def randCrossover():	#recombinação de ponto local discreto
	global N

	new_individual = ([], [])
	for i in range(0, N):
		index = random.randint(0,1)
		new_individual[0].append(parents[index][0][i])
		new_individual[1].append(parents[index][1][i])
	
	return new_individual


def meanCrossover(parents):	#recombinação de ponto local intermediário
	global N

	new_individual = ([], [])
	for i in range(0, N):
		new_individual[0].append( (parents[0][0][i] + parents[1][0][i])/2.0 )
		new_individual[1].append( (parents[0][1][i] + parents[1][1][i])/2.0 )
		
	return new_individual


def crossover(parents):	#função direciona para o tipo de crossover 
	global CROSSOVER_OPTION
	global DEBUG
	global debug_file
	new_individual = ([], [])

	if(CROSSOVER_OPTION == "rand"):
		new_individual = randCrossover(parents)

	elif(CROSSOVER_OPTION == "mean"):
		new_individual = meanCrossover(parents)

	if(DEBUG):
		debug_file.write("individuo crossover: " + str(new_individual) + "\n\n")
	survivalSelection(new_individual)
	return


def parentsSelection( ):	#seleção de pais aleatória
	global POPULATION
	global POP_SIZE
	global debug_file
	lamdba = 2 #quantidade de pais a serem escolhidos.
	parents = []

	while len(parents) < lamdba:
		index = random.randint(0, POP_SIZE-1)

		if POPULATION[index] not in parents:
			parents.append(POPULATION[index])		


	if(DEBUG):
		debug_file.write("pais: " + str(parents))
		debug_file.write("\n\n")


	crossover(parents)

	return


def mutation():
	global POPULATION
	global MUTATION_OPTION

	if MUTATION_OPTION == 1:
		POPULATION = mutationCase1()

	elif MUTATION_OPTION == 2:
		POPULATION = mutationCase2()

	return


def mutationCase1():	#mutação usando caso 1
	global E_0
	global POPULATION
	global N
	global debug_file
	
	new_population = []

	tau = 1/math.sqrt(N)

	for individual in POPULATION:
		sig_line = []
		new_individual = []

		dev = random.gauss(0, 1)
		
		if(DEBUG):
			debug_file.write("DEV: %f; TAU: %f; EXP: %f;\n" %(dev, tau, math.exp(tau*dev)))

		for sig in individual[1]:
			aux = sig*math.exp(tau*dev)
			if( aux < E_0 ):
				sig_line.append(E_0)
			else:
				sig_line.append(aux)


		i = 0
		dev = random.gauss(0, 1)
		for gene in individual[0]:
			new_individual.append( gene + (sig_line[i]*random.gauss(0, 1)) )
			i = i + 1

		
		if( fitness(new_individual) < fitness(individual[0]) ):
			if(DEBUG):
				debug_file.write("PAR: (" + str(new_individual) + "," + str(sig_line) + ")\n")
			#individual = (new_individual, sig_line)
			new_population.append( (new_individual, sig_line) )

		else:
			new_population.append( (individual[0], individual[1]) )
			#individual = (individual[0], sig_line)

	return new_population


def mutationCase2():	#mutação usando caso 2
	global E_0
	global POPULATION
	global N
	global c
	global c2

	new_population = []

	tau = c*1/math.sqrt((2*math.sqrt(N)))
	tau_line = c2*1/math.sqrt(2*N)
	
	for individual in POPULATION:
		sig_line = []
		new_individual = []

		dev = random.gauss(0, 1)

		for sig in individual[1]:
			aux = sig*math.exp( (tau_line*dev) + (tau*random.gauss(0,1) ))
			if( aux < E_0 ):
				sig_line.append(E_0)
			else:
				sig_line.append(aux)


		i = 0
		for gene in individual[0]:
			new_individual.append( gene + (sig_line[i]*random.gauss(0,1)) )
			i = i + 1

		if( fitness(new_individual) < fitness(individual[0]) ):
			new_population.append( (new_individual, sig_line) )
		
		else:
			new_population.append( (individual[0], individual[1]) )
	
	return new_population


def genPopulation():	#função que gera a população inicial
	global POP_SIZE
	global POPULATION
	global MUTATION_OPTION
	global E_0
	global N
	POPULATION = []

	for i in range(0, POP_SIZE):
		sig = random.uniform(E_0, 5)
		POPULATION.append( ([], []) )

		for j in range(0, N):
			POPULATION[i][0].append(random.uniform(-15,15))
			if( MUTATION_OPTION == 2 ):
				POPULATION[i][1].append(random.uniform(E_0, 5))
			else:
				#POPULATION[i][1].append(random.uniform(E_0, 5))
				POPULATION[i][1].append(sig)
	return



def main():
	global REPEAT
	global POPULATION
	global MUTATION_OPTION
	global POP_SIZE
	global DEBUG
	global debug_file
	global min_file
	global mean_file
	global c 
	global c2
	
	stop_flag = False
	conv = 0
	conv_list = []


	for j in range(0, REPEAT):
		genPopulation()
		
		if(DEBUG):
			debug_file.write("na geração: ")
			printAllFitness()
			debug_file.write("\n\n")


		i = 0
		stop_flag = False

		while i < 4000:
		
			parentsSelection()
			mutation()

			if( (min([fitness(x) for x,y in POPULATION ]) < 0.00001) and (stop_flag == False) ):
				print("ACABOU EM %d" %(i))
				conv_list.append(i)
				stop_flag = True
				conv = conv + 1
			

			if(DEBUG):
				debug_file.write("depois da mutação: ")
				printAllFitness()
				debug_file.write("\n\n")

			mean_file.write("%f\n" %(numpy.mean([fitness(x) for x,y in POPULATION])))
			output_file.write("%f\n" %(min([fitness(x) for x,y in POPULATION])))
			i = i + 1

		for i in range(0, POP_SIZE):
			if(DEBUG):
				debug_file.write("%d: %f\n" %(i, fitness(POPULATION[i][0])))	
			else:
				output_file.write("%d: %f\n" %(i, fitness(POPULATION[i][0])))

		if(stop_flag == False):
			print("Algoritmo não convergiu...\n")

	output_file.write("\n\n")
	if(stop_flag == True):
		output_file.write("Média de iterações para convergencia: %f\n" %(numpy.mean(conv_list)))
	output_file.write("Vezes convergidas: %d\n" %(conv))
	output_file.write(str(conv_list))

if __name__ == "__main__":
	main()

